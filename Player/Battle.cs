﻿using Player.Spells;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Player
{
    class Battle
    {
        private Team PlayerTeam { get; set; }
        private Team AITeam { get; set; }
        private Queue<Actor> Order { get; set; }

        public Battle(Team t1, Team t2)
        {
            PlayerTeam = t1;
            AITeam = t2;
            Order = new Queue<Actor>();
            decideTurnOrder();
        }
        public void Start()
        {
            do
            {
                var CurrentPlayer = Order.Dequeue();
                TurnStart(ref CurrentPlayer);

                if (!CurrentPlayer.Available)
                    continue;

                if (CurrentPlayer.CurrentHealth > 0)
                    Order.Enqueue(CurrentPlayer);

                switch (CurrentPlayer.TakeTurn())
                {
                    case EAction.DoNothing:
                        // Dont need to do anything but just leaving here for expansion
                        break;
                    case EAction.Run:
                        // Exit function
                        return;
                    case EAction.Ability:
                        ISpell spell = CurrentPlayer.PickAbility();
                        Actor Target = CurrentPlayer.PickTarget();
                        spell.Behaviour(ref CurrentPlayer, ref Target);
                        break;
                }
            } while (PlayerTeam.Health != 0 && AITeam.Health != 0);
        }
        public void TurnStart(ref Actor player)
        {
            if (player.burning > 0)
            {
                player.setHealth(player.CurrentHealth - (5 * player.burning));
                player.burning--;
            }
            if (player.electrified > 0)
            {
                player.setHealth(player.CurrentHealth - player.electrified);
                player.electrified--;
            }
        }
        protected void decideTurnOrder()
        {
            throw new NotImplementedException();
            //Order.Enqueue();
        }
    }
}
