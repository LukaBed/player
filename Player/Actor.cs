﻿using static Player.Battle;

namespace Player
{
    public abstract class Actor
    {
        public ISpell[] Spells = new ISpell[4];
        public string Name { get; private set; }
        public int CurrentHealth { get; protected set; }
        public abstract void setHealth(int value);
        public int Vitality { get; protected set; }
        public int LifePercent { get; protected set; }
        public int Int { get; protected set; }
        public abstract int GetMaxHealth();
        public int burning { get; set; }
        public int electrified { get; set; }
        public int stun { get; set; }
        public abstract EAction TakeTurn();
        public bool Available { 
            get 
            {
                return stun == 0;
            }
        }
        public Actor(string name)
        {
            Name = name;
            Vitality = 50;
            CurrentHealth = GetMaxHealth();
        }
        internal abstract ISpell PickAbility();
        internal abstract Actor PickTarget();
    }
}