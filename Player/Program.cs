﻿using Player.Spells;
using System;

namespace Player
{
    class Program
    {
        static void Main(string[] args)
        {
            var fac = new SpellFactory();
            var t1 = new Team();
            var t2 = new Team();

            var p1 = new Player("Josh");
            p1.Spells[0] = fac.Make(ESpell.DamageSpell);
            p1.Spells[1] = fac.Make(ESpell.HealSpell);
            p1.Spells[2] = fac.Make(ESpell.FireSpell);
            p1.Spells[3] = fac.Make(ESpell.HealSpell);

            var p2 = new Player("NME");
            p2.Spells[0] = fac.Make(ESpell.DamageSpell);
            p2.Spells[1] = fac.Make(ESpell.HealSpell);
            p2.Spells[2] = fac.Make(ESpell.FireSpell);
            p2.Spells[3] = fac.Make(ESpell.HealSpell);

            t1.Add(p1);
            t2.Add(p2);

            var btl = new Battle(t1, t2);

            btl.Start();
            Console.ReadKey();
        }
    }
}
