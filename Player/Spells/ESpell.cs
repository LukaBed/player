﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Player.Spells
{
    public enum ESpell
    {
        HealSpell,
        FireSpell,
        DamageSpell
    }
}
