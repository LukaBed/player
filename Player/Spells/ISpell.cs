﻿namespace Player
{
    public interface ISpell
    {
        string name { get; set; }
        void Behaviour(ref Actor user, ref Actor target);
    }
}