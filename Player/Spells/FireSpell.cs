﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Player.Spells
{
    public class FireSpell : ISpell
    {
        public string name { get; set; }

        public void Behaviour(ref Actor user, ref Actor target)
        {
            int damage = 10;
            damage += user.Int;
            target.setHealth(target.CurrentHealth - damage);
            target.burning++;
        }
    }
}
