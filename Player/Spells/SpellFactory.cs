﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Player.Spells
{
    class SpellFactory
    {
        //ESpell type
        public ISpell Make(ESpell type)
        {
            switch(type)
            {
                case ESpell.HealSpell:
                    return new HealSpell() { name = "Smol Heal"};
                case ESpell.FireSpell:
                    return new FireSpell() { name = "Heck" };
                case ESpell.DamageSpell:
                    return new FireSpell() { name = "Smack" };
                default:
                    throw new Exception("Invalid enum passed to spell factory");
            }
        }
    }
}
