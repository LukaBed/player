﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Player.Spells
{
    public class HealSpell : ISpell
    {
        public string name { get; set; }
        public void Behaviour(ref Actor user, ref Actor target)
        {
            int heal = 20;
            heal += user.Int;
            target.setHealth(target.CurrentHealth + heal);
        }
    }
}
