﻿using System;

namespace Player
{
    class Player : Actor
    {

        public Player(string name) : base(name)
        {
        }

        public override void setHealth(int value)
        {
            if (value < 0) CurrentHealth = 0;
            else if (value > GetMaxHealth()) CurrentHealth = GetMaxHealth();
            else CurrentHealth = value;
        }
        public void LevelUp()
        {
            Vitality += 5;
        }
        public override int GetMaxHealth()
        {
            return Vitality * 5;
        }

        public override EAction TakeTurn()
        {
            throw new NotImplementedException();
            //Write options
            //Find out what option needed
            //Do call/do that option?
            // How do we do the option without a target?
        }

        internal override ISpell PickAbility()
        {
            throw new NotImplementedException();
        }

        internal override Actor PickTarget()
        {
            throw new NotImplementedException();
        }
    }
}
