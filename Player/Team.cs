﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Player
{
    class Team
    {
        public List<Actor> Members { get; internal set; }
        public ETeam Type {get; private set;}
        public Team()
        {
            Members = new List<Actor>();
        }

        public int Health
        {
            get
            {
                return Members.Sum(member => member.CurrentHealth);
            }
        }
        public void Add(Actor a)
        {
            Members.Add(a);
        }
    }
}
